# coding=utf-8


import logging
import re

from hgeocode.exceptions import SuspiciousOperation
from hgeocode import conf

logger = logging.getLogger('geocoder')


class GeocoderConfig(object):
    """Geocoder configuration object."""

    def __init__(self, fias_corrector=None, name_corrector=None, levenshtein_distance=None):
        self.levenshtein_distance = levenshtein_distance
        self.name_corrector = name_corrector
        self.fias_corrector = fias_corrector


class GeocoderStats(object):
    """Geocoder Statistics writer interface."""

    @staticmethod
    def write_fias_result(val):
        """Log fias event."""
        logger.debug('FIAS result: [%s]' % val)

    @staticmethod
    def write_fixed_found(val):
        """Log fixed-found event."""
        logger.debug('fixed found: [%s]' % val)

    @staticmethod
    def write_notfound_group(args):
        """Log not-found event."""
        logger.debug('not found group: [%s]' % (';'.join(args) + '\n'))


class SearchPattern(object):
    """Helper class for building search string from parts."""

    def __init__(self, *parts, **kwargs):
        self.parts = parts
        self.neighbour = kwargs.get('neighbour', None)

    def __add__(self, other):
        if not isinstance(other, SearchPattern):
            return NotImplemented
        return SearchPattern(*(self.parts + other.parts),
                             neighbour=self.neighbour or other.neighbour)

    def to_string(self):
        """Convert parameters list into strong."""
        return ', '.join(self.parts)

    def __unicode__(self):
        return self.to_string()


class SearchData(object):
    """Search data object. Used for storing in search results."""

    def __init__(self, config, stats, **kwargs):
        self.config = config
        self.stats = stats
        self.address = kwargs.get('address')
        self.town = kwargs.get('town', '').strip()
        self.street = kwargs.get('street', '').strip()
        self.house = kwargs.get('house', '').strip()
        self.build = kwargs.get('build')
        self.postalcode = kwargs.get('postalcode')
        self.type = kwargs.get('type')

        self.is_fias_search_done = False

        self._frozen_street = self.street
        self._frozen_name = ' '.join([self.town] +
                                     ([self.address]
                                      if self.address is not None
                                      else [self.street, self.house]))

    def generate_region_identifiers(self):
        """Region identified by postal code or town."""
        yield self.town
        if self.postalcode:
            yield self.postalcode

    def extract_litera(self, address=None):
        """Try to extract litera from address."""
        address = self.address if not address else address
        for pattern in conf.ADDRESS_LITERA:
            parts = re.split(pattern, address)
            if len(parts) == 2:
                address = parts[0]
                break
        return address

    def cleaned_address(self, address=None):
        """
        Try to clean up address.

        Remove some parts and perform some
        substitutions.

        :return: string with cleaned address.
        """
        address = self.address if not address else address
        for pattern, replacement in conf.ADDRESS_SUBSTITUTIONS.items():
            address = re.sub(pattern, replacement, address, 0, re.IGNORECASE | re.UNICODE)
        for pattern, replacement in conf.STREET_SUBSTITUTIONS.items():
            address = re.sub(pattern, replacement, address, 0, re.IGNORECASE | re.UNICODE)
        return address.strip()

    @staticmethod
    def extract_house(address):
        """Try to clean up address."""
        if re.search('[\d]+/[\d]+', address):
            return re.sub('/[\d]+', '', address)
        return address

    @staticmethod
    def extract_building(address):
        """Try to delete building number from address string."""
        if re.search(r'\bк\d+', address, re.IGNORECASE | re.UNICODE):
            return re.sub(r'\bк\d+', '', address, 0, re.IGNORECASE | re.UNICODE)
        return address

    def correct_town(self):
        """Check if town is correct."""
        if not self.town:
            return False
        if not self.config.name_corrector:  # Check if OSM corrector is defined
            return True
        # Check town
        town, is_cached = self.config.name_corrector.town(self.town)
        if not town:
            logger.debug('[%s] town not found' % self.town)
            if not is_cached:
                self.stats.write_notfound_group([self.town])
            return False
        self.town = town
        return True

    def correct_street(self):
        """Check if street is correct."""
        if not self.town or not self.street:
            return False
        if self.config.fias_corrector:  # If FIAS is enabled use it
            found_name, is_cached = self.config.fias_corrector.street(self.town, self.street)
            if found_name:
                self.is_fias_search_done = True
                if not is_cached and found_name != self.street:
                    self.stats.write_fias_result(('%s -> %s \n' %
                                                  (self._frozen_street, found_name)))
                self.street = found_name
        if not self.config.name_corrector:  # Check if OSM corrector is defined
            return True
        # Check street
        street, is_cached = self.config.name_corrector.street(self.town, self.street)
        if not street:
            logger.debug('[%s, %s] street not found' % (self.town, self._frozen_street))
            if not is_cached:
                self.stats.write_notfound_group([self.town, self._frozen_street])
            return False
        self.street = street
        return True

    def gen_house_patterns(self, address):
        """Try to detect house from address string."""
        yield SearchPattern(address)
        cleaned_address = self.cleaned_address(address)
        if cleaned_address != address:
            yield SearchPattern(cleaned_address)
        ex_house = self.extract_house(address)
        if ex_house != address:
            yield SearchPattern(ex_house)
            cleaned_address = self.cleaned_address(ex_house)
            if cleaned_address != ex_house:
                yield SearchPattern(cleaned_address)
        ex_litera = self.extract_litera(address)
        if ex_litera != address:
            yield SearchPattern(ex_litera)
            cleaned_address = self.cleaned_address(ex_litera)
            if cleaned_address != ex_litera:
                yield SearchPattern(cleaned_address)
            ex_house = self.extract_house(ex_litera)
            if ex_house != ex_litera:
                yield SearchPattern(ex_house)
                cleaned_address = self.cleaned_address(ex_house)
                if cleaned_address != ex_house:
                    yield SearchPattern(cleaned_address)
            ex_building = self.extract_building(cleaned_address)
            if ex_building != cleaned_address:
                yield SearchPattern(ex_building)
        # Discard all after house number
        address = self.cleaned_address(address)
        m = re.match(r'^(.*?)([\d]+).*', address)
        if m:
            prefix = m.group(1)
            number = int(m.group(2))
            # If address has some digits then it is house number.
            # Try use this number and neighbour numbers.
            if cleaned_address != prefix + str(number):
                yield SearchPattern(prefix, str(number))
                yield SearchPattern(str(number), prefix)
            for i in (-2, 2, -4, 4, -6, 6):
                if number + i > 0:
                    yield SearchPattern(prefix, str(number + i), neighbour=i)
                    yield SearchPattern(str(number + i), prefix, neighbour=i)

    @staticmethod
    def split_address(address):
        """Generate candidates for street and house by given address."""
        sep = ',0123456789'
        prev = 0
        for i in range(2, len(address) - 1):
            if address[i] in sep and prev < i - 1:
                yield address[:i], address[i:]

    def build_patterns_from_parts(self, region_identifier):
        """Build patterns from parts."""
        if region_identifier and self.street and self.house:  # build address from parts.
            if self.build:
                yield SearchPattern(self.house + ' к' + self.build,
                                    self.street, region_identifier)
                yield SearchPattern(region_identifier, self.street,
                                    self.house + ' к' + self.build)
            for pattern in self.gen_house_patterns(self.house):
                yield pattern + SearchPattern(self.street, region_identifier)
                yield SearchPattern(region_identifier, self.street) + pattern

    def generate_patterns(self):
        """Generate pattern from attrs."""
        if self.correct_town():
            if self.correct_street():
                # We have correct 'town' and 'street' fields
                for region_identifier in self.generate_region_identifiers():
                    for pattern in self.build_patterns_from_parts(region_identifier):
                        yield pattern
            elif self.address:  # If address is defined use it.
                # Try to split address into street and house.
                for street, house in self.split_address(self.address):
                    self.street = street
                    if self._frozen_street != street and self.correct_street():
                        # Bingo
                        self.house = house
                        for pattern in self.build_patterns_from_parts(self.town):
                            yield pattern

        # We don't know what do our fields mean.
        # Let's treat it as one string.
        address = ''
        address += self.town or ''
        if self.address:
            address += self.address
        else:
            address += (self.street or '') + (self.house or '')
        address = self.cleaned_address(address)
        yield SearchPattern(address)


class SearchResult(object):
    """Search result class."""

    def __init__(self):
        self.is_street = False
        self.is_applied = False
        self.pattern = None
        self.left_pattern = None
        self.right_pattern = None
        self.search_data = None
        self.left_result_data = None
        self.right_result_data = None
        self.result_data = None

    def apply(self, search_data, result_data, pattern, is_street=False):
        """Save search data, results and used pattern."""
        self.search_data = search_data
        self.is_applied = True
        if pattern.neighbour and not is_street:  # Neighbour house found
            if pattern.neighbour < 0 and not self.left_pattern:
                self.left_pattern = pattern
                self.left_result_data = result_data
            if pattern.neighbour > 0 and not self.right_pattern:
                self.right_pattern = pattern
                self.right_result_data = result_data
        else:
            self.result_data = result_data
            self.pattern = pattern
            self.is_street = is_street
        return not is_street and not pattern.neighbour  # Do further search or not

    def finalize(self):
        """Get exact coordinates or calculate approx, depending what result was found."""
        if not self.is_applied:
            raise SuspiciousOperation('Cannot finalize unapplied search result')

        if self.pattern and not self.is_street:  # Exact house found
            self.search_data.stats.write_fixed_found(
                '[%s] -> [%s] -> (%s, %s)\n' %
                (self.search_data._frozen_name, self.pattern.to_string(),
                 self.result_data['lat'], self.result_data['lon']))
        elif self.left_pattern and self.right_pattern:  # Both left and right houses were found.
            lat = (self.right_result_data['lat'] * self.right_pattern.neighbour -
                   self.left_result_data['lat'] * self.left_pattern.neighbour) / \
                  (self.right_pattern.neighbour - self.left_pattern.neighbour)
            lon = (self.right_result_data['lon'] * self.right_pattern.neighbour -
                   self.left_result_data['lon'] * self.left_pattern.neighbour) / \
                  (self.right_pattern.neighbour - self.left_pattern.neighbour)
            self.result_data = self.left_result_data
            self.pattern = self.left_pattern
            self.result_data['lat'], self.result_data['lon'] = lat, lon
            self.search_data.stats.write_fixed_found(
                '[%s] -> between [%s] and [%s] -> (%s, %s)\n' %
                (self.search_data._frozen_name, self.left_pattern.to_string(), self.right_pattern.to_string(),
                 self.result_data['lat'], self.result_data['lon']))
        elif self.left_pattern:  # Only left house was found
            self.result_data = self.left_result_data
            self.pattern = self.left_pattern
            self.search_data.stats.write_fixed_found(
                '[%s] -> near [%s] -> (%s, %s)\n' %
                (self.search_data._frozen_name, self.pattern.to_string(),
                 self.result_data['lat'], self.result_data['lon']))
        elif self.right_pattern:  # Only right house was found
            self.result_data = self.right_result_data
            self.pattern = self.right_pattern
            self.search_data.stats.write_fixed_found(
                '[%s] -> near [%s] -> (%s, %s)\n' %
                (self.search_data._frozen_name, self.pattern.to_string(),
                 self.result_data['lat'], self.result_data['lon']))
        else:  # Only street was found
            self.search_data.stats.write_fixed_found(
                '[%s] -> [%s] -> %s(%s, %s)\n' %
                (self.search_data._frozen_name, self.pattern.to_string(),
                 'street ' if self.is_street else '',
                 self.result_data['lat'], self.result_data['lon']))
        return self

    def get_data(self):
        """Extract coordinates from results."""
        return {
            'lat': float(self.result_data['lat']),
            'lng': float(self.result_data['lon']),
        }


class BaseGeocoder(object):
    """Superclass of all geocoders. Implementations are located in 'backends' module."""

    def __init__(self, config, stats):
        """
        Initialize geocoder instance by config.

        :param config: GeocoderConfig instance.
        :param stats: GeocoderStats instance.
        """
        self.config = config
        self.stats = stats

    def geocode(self, **kwargs):
        """
        Geocoder entry point. This method must be implemented in descendants.

        :param kwargs: Geographical object names and attributes like 'address'.
        :return: Finalized SearchResult object.
        """
        raise NotImplementedError
