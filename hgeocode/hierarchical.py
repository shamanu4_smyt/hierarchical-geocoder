"""
Hierarchical geocoder backend.

Uses HGeocode for full text search over Nominatim's database.
"""

import json
import logging

from hgeocode.exceptions import AddressNotFound
from hgeocode.geocoder import SearchPattern, SearchData, SearchResult, BaseGeocoder
from hgeocode.nominatim import HGeocode

logger = logging.getLogger('geocoder')


class Geocoder(BaseGeocoder):
    """Hierarchical geocoder."""

    def __init__(self, config, stats):
        super(Geocoder, self).__init__(config, stats)

        self._geocode = HGeocode()
        self._geocode.include_polygon_as_geo_json = True

    def geocode(self, **kwargs):
        """Use kwargs to produce a query string and put it to HGeocode."""
        search_data = SearchData(self.config, self.stats, **kwargs)
        result = SearchResult()

        query = ' '.join([kwargs.get('town')] +
                         ([kwargs.get('address')]
                          if kwargs.get('address') is not None
                          else [kwargs.get('street'), kwargs.get('house')]))
        if kwargs.get('build'):
            query += ' ' + kwargs.get('build')
        if kwargs.get('postalcode'):
            query += ' ' + kwargs.get('postalcode')

        pattern = SearchPattern(query)

        logger.debug('Hierarchical search: [%s]' % query)
        data = self._geocode.search(**kwargs)
        for item in data:
            item['geojson'] = json.loads(item['geojson'])
        logger.debug('Hierarchical search: [%s], result: [%s]' % (query,
                                                                  '|'.join([
                                                                      str(item['lat']) + ',' + str(item['lon']) + ':' +
                                                                      item['class'] + '-' + item['type']
                                                                      for item in data
                                                                  ])))
        if data:
            logger.info('Query: [%s], found: [%s]' % (query, data[0]['display_name']))
            result.apply(search_data, data[0], pattern)
            return result.finalize()
        else:
            raise AddressNotFound()
