class SuspiciousOperation(Exception):
    """The user did something suspicious."""


class GeocoderException(Exception):
    """Geocoding failure."""


class ValueNotFound(GeocoderException):
    """Value not found while geocoding."""


class AddressNotFound(GeocoderException):
    """Address not found while geocoding."""

    def __init__(self, is_long_street_found=False, ignored_street=False, *args, **kwargs):
        super(AddressNotFound, self).__init__(*args, **kwargs)
        self.is_long_street_found = is_long_street_found
        self.ignored_street = ignored_street
