"""Geocoding result formatting routines."""

from collections import OrderedDict

from hgeocode import conf
from hgeocode import db

DEFAULT_FORMAT = {
    'include_polygon_as_points': False,
    'include_polygon_as_text': False,
    'include_polygon_as_geo_json': True,
    'include_polygon_as_kml': False,
    'include_polygon_as_svg': False,
}


def get_details(place_ids, lang_pref_order):
    """Get information from nominatim database for places by id."""
    if not place_ids:
        return []

    dbc = db.DatabaseConnection()

    language_pref_array_sql = "ARRAY[" + \
                              ','.join([str(dbc.adapt(l)) for l in lang_pref_order]) + \
                              "]"

    # Get the details for display (is this a redundant extra step?)
    s_place_ids = ','.join([str(dbc.adapt(p_id)) for p_id in place_ids])

    importance_sql = '1 *'

    sql = """
    SELECT   osm_type,
             osm_id,
             class,
             type,
             admin_level,
             rank_search,
             rank_address,
             Min(place_id)                                                        AS place_id,
             Min(parent_place_id)                                                 AS parent_place_id,
             calculated_country_code                                              AS country_code,
             get_address_by_language(place_id, %(language_pref_array)s)           AS langaddress,
             get_name_by_language(name, %(language_pref_array)s)                  AS placename,
             get_name_by_language(name, array['ref'])                             AS ref,
             avg(st_x(centroid))                                                  AS lon,
             avg(st_y(centroid))                                                  AS lat,
             %(importance_sql)s coalesce(importance,0.75-(RANK_SEARCH::FLOAT/40)) AS importance,
             (
                    SELECT max(p.importance*(p.rank_address+2))
                    FROM   place_addressline s,
                           placex p
                    WHERE  s.place_id = min(
                           CASE
                                  WHEN placex.rank_search < 28 THEN placex.place_id
                                  ELSE placex.parent_place_id
                           end)
                    AND    p.place_id = s.address_place_id
                    AND    s.isaddress
                    AND    p.importance IS NOT NULL) AS addressimportance,
             (extratags->'place')                    AS extra_place
    FROM     placex
    WHERE    place_id IN (%(s_place_ids)s)
    AND      (
                      placex.rank_address BETWEEN %(min_address_rank)s AND      %(max_address_rank)s
             OR       (
                               extratags->'place') = 'city')
    AND      linked_place_id IS NULL
    GROUP BY osm_type,
             osm_id,
             class,
             type,
             admin_level,
             rank_search,
             rank_address,
             calculated_country_code,
             importance ,
             place_id,
             langaddress,
             placename,
             ref,
             extratags->'place'
    UNION
    SELECT   'T'                                                        AS osm_type,
             place_id                                                   AS osm_id,
             'place'                                                    AS class,
             'house'                                                    AS type,
             NULL                                                       AS admin_level,
             30                                                         AS rank_search,
             30                                                         AS rank_address,
             min(place_id)                                              AS place_id,
             min(parent_place_id)                                       AS parent_place_id,
             'us'                                                       AS country_code,
             get_address_by_language(place_id, %(language_pref_array)s) AS langaddress,
             NULL                                                       AS placename,
             NULL                                                       AS ref,
             avg(st_x(centroid))                                        AS lon,
             avg(st_y(centroid))                                        AS lat,
             %(importance_sql)s -1.15                                   AS importance,
             (
                    SELECT max(p.importance*(p.rank_address+2))
                    FROM   place_addressline s,
                           placex p
                    WHERE  s.place_id = min(location_property_tiger.parent_place_id)
                    AND    p.place_id = s.address_place_id
                    AND    s.isaddress
                    AND    p.importance IS NOT NULL) AS addressimportance,
             NULL                                    AS extra_place
    FROM     location_property_tiger
    WHERE    place_id IN (%(s_place_ids)s)
    AND      30 BETWEEN %(min_address_rank)s AND      %(max_address_rank)s
    GROUP BY place_id ,
             place_id
    UNION
    SELECT   'L'                                                        AS osm_type,
             place_id                                                   AS osm_id,
             'place'                                                    AS class,
             'house'                                                    AS type,
             NULL                                                       AS admin_level,
             30                                                         AS rank_search,
             30                                                         AS rank_address,
             min(place_id)                                              AS place_id,
             min(parent_place_id)                                       AS parent_place_id,
             'us'                                                       AS country_code,
             get_address_by_language(place_id, %(language_pref_array)s) AS langaddress,
             NULL                                                       AS placename,
             NULL                                                       AS ref,
             avg(st_x(centroid))                                        AS lon,
             avg(st_y(centroid))                                        AS lat,
             %(importance_sql)s -1.10                                   AS importance,
             (
                    SELECT max(p.importance*(p.rank_address+2))
                    FROM   place_addressline s,
                           placex p
                    WHERE  s.place_id = min(location_property_aux.parent_place_id)
                    AND    p.place_id = s.address_place_id
                    AND    s.isaddress
                    AND    p.importance IS NOT NULL) AS addressimportance,
             NULL                                    AS extra_place
    FROM     location_property_aux
    WHERE    place_id IN (%(s_place_ids)s)
    AND      30 BETWEEN %(min_address_rank)s AND      %(max_address_rank)s
    GROUP BY place_id ,
             place_id,
             get_address_by_language(place_id, %(language_pref_array)s)
    ORDER BY importance DESC
    """

    sql = sql % {
        'language_pref_array': language_pref_array_sql,
        'importance_sql': importance_sql,
        's_place_ids': s_place_ids,
        'min_address_rank': 0,  # self.min_address_rank,
        'max_address_rank': 30,  # self.max_address_rank
    }

    search_results = dbc.dict_array(sql)

    # Return details in the same order as place_ids
    ordered_results = OrderedDict()
    for place_id in place_ids:
        ordered_results[place_id] = None
    for result in search_results:
        ordered_results[result['place_id']] = result
    search_results = list(ordered_results.values())

    return [r for r in search_results if r]


def format_results(search_results, fmt=DEFAULT_FORMAT):
    """Format result display name and coordinates."""
    for result in search_results:
        if conf.SEARCH_AREA_POLYGONS:
            # Get the bounding box and outline polygon
            sql = """
                  select place_id, 0 as numfeatures, st_area(geometry) as area,
                         ST_Y(centroid) as centrelat, ST_X(centroid) as centrelon,
                         ST_YMin(geometry) as minlat, ST_YMax(geometry) as maxlat,
                         ST_XMin(geometry) as minlon, ST_XMax(geometry) as maxlon
                  """
            if fmt.get('include_polygon_as_geo_json'):
                sql += ", ST_AsGeoJSON(geometry) as asgeojson"
            if fmt.get('include_polygon_as_kml'):
                sql += ", ST_AsKML(geometry) as askml"
            if fmt.get('include_polygon_as_svg'):
                sql += ", ST_AsSVG(geometry) as assvg"
            if fmt.get('include_polygon_as_text') or fmt.get('include_polygon_as_points'):
                sql += ", ST_AsText(geometry) as astext"
            sql += " from placex where place_id = " + str(result['place_id'])
            dbc = db.DatabaseConnection()
            point_polygon = dbc.row_as_dict(sql)

            if point_polygon:
                if fmt.get('include_polygon_as_geo_json'):
                    result['geojson'] = point_polygon['asgeojson']
                if fmt.get('include_polygon_as_kml'):
                    result['kml'] = point_polygon['askml']
                if fmt.get('include_polygon_as_svg'):
                    result['svg'] = point_polygon['assvg']
                if fmt.get('include_polygon_as_text'):
                    result['text'] = point_polygon['astext']

                if point_polygon['centrelon'] is not None and point_polygon['centrelat'] is not None:
                    result['lat'] = point_polygon['centrelat']
                    result['lon'] = point_polygon['centrelon']

                result['bounding_box'] = [point_polygon['minlat'], point_polygon['maxlat'],
                                          point_polygon['minlon'], point_polygon['maxlon']]

        result['display_name'] = result['langaddress']
        del result['langaddress']

    return search_results
