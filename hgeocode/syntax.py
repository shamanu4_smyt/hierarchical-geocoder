# encoding: utf-8
"""Syntactic analysis of address lines."""

ORDINAL_ENDINGS = frozenset([
    'й', 'ый', 'ой', 'ий', 'я', 'ая', 'е', 'ое',
    'st', 'nd', 'rd', 'th'
])
ORDINAL_SEPARATORS = ['', '-']
HOUSE_NUMBER_SEPARATORS = ['/']
HOUSE_NUMBER_WORDS = frozenset([
    'корпус', 'корп', 'кор',
    'литера', 'литер', 'лит',
    'строение', 'строен', 'стр'
])
ADDRESS_SEPARATORS = ['-']
PART_SEPARATORS = [',']
BASE_PROBABILITY = 0.25
SYNONYMS = {
    'линия': {'линии'},
    'линии': {'линия'},
}
ABBREVIATIONS = {
    'дор': {'дорога'},
    'пер': {'переулок'},
    'ул': {'улица'},
    'наб': {'набережная'},
    'пл': {'площадь'},
    'пр': {'проспект', 'проезд'},
    'просп': {'проспект'},
    'пр-кт': {'проспект'},
    'бул': {'бульвар'},
    'бульв': {'бульвар'},
    'б-р': {'бульвар'},
    'кан': {'канал'},
    'пос': {'поселок'},
}


def is_ordinal(w1, sep, w2):
    """Check if given ags form an ordinal numerator."""
    return w1.isdigit() and sep.strip() in ORDINAL_SEPARATORS and w2.lower() in ORDINAL_ENDINGS


def analyze(words):
    """Analyze list of words and set importance level values etc."""
    def get_word(idx):
        if 0 <= idx < len(words):
            return words[idx].original
        return ''

    # Calculate 'town', 'street' and 'house' importance value for every word
    state = 's'  # mode - town, street or house
    for index, word in enumerate(words):
        if any(char in PART_SEPARATORS for char in word.prev):
            state = 's'
        if word.original.isdigit():
            next_word = get_word(index + 1)
            if is_ordinal(word.original, word.next, next_word):  # 4th avenue
                word.street = 1
                state = 's'
            elif get_word(index - 1).isalpha() and word.prev in ADDRESS_SEPARATORS:  # arzamas-16
                word.street = 1
                state = 's'
            elif len(word.original) >= 4:  # Postal code
                word.street = 1
                state = 's'
            else:
                word.house = 1
                state = 'h'
        if word.original.isalpha():
            if len(word.original) > 2 and word.places.max_rank and \
                    word.original.lower() not in HOUSE_NUMBER_WORDS:
                if word.places.min_rank <= 12:
                    # It is likely the town name
                    word.town = 1 - BASE_PROBABILITY
                    word.street = BASE_PROBABILITY
                    state = 't'
                else:
                    # This word is likely the street name
                    word.street = 1
                    state = 's'
            else:
                if state == 'h':
                    word.house = 1 - BASE_PROBABILITY
                    word.street = BASE_PROBABILITY
                else:
                    word.street = 1 - BASE_PROBABILITY
                    word.house = BASE_PROBABILITY


def synonyms(word, abbreviation=False):
    """Return pre-defined synonym for given word."""
    w = word.lower()
    result = SYNONYMS[w] if w in SYNONYMS else frozenset()
    if abbreviation and w in ABBREVIATIONS:
        result |= ABBREVIATIONS[w]
    return result
