# coding=utf-8

import logging
import re

from hgeocode import conf
from hgeocode.geocoder import GeocoderConfig, GeocoderStats

logger = logging.getLogger('geocoder')


class BaseNameCorrector(object):
    """Name correctors interface."""

    def town(self, town):
        """
        Spellcheck town name.

        :return: Correct town name if found. None if not.
        """
        raise NotImplementedError

    def street(self, town, street):
        """
        Spellcheck street name.

        :return: Correct street name if found. None if not.
        """
        raise NotImplementedError


class NameCorrector(BaseNameCorrector):
    """Intermediate name-corrector interface class."""

    TOWN = 'town'
    STREET = 'street'

    def __init__(self):
        self.town_cache = {}
        self.street_cache = {}

    @staticmethod
    def extract_part(street, possibles, aliases, boundary=True):
        """
        Extract parts from address.

        :param street: street string
        :param possibles: regexp to extract
        :param aliases: replace found with alias
        :param boundary: flag how to represent possibles regexps with boundary or w/o it
        :return: tuple, extracted part and remainder from street
        """
        part = None
        if boundary:
            pattern = r'\b%s\b'
        else:
            pattern = r'%s'
        for possible in possibles:
            res = re.search(pattern % possible, street, re.IGNORECASE | re.UNICODE)
            if res:
                part = res.group().lower()
                street = street[:res.start()] + street[res.end():]
                break

        if part:
            # replace alias with canonical value:
            for alias_list, value in list(aliases.items()):
                if part in alias_list:
                    part = value
                    break

        return part, street.strip('. ')

    @staticmethod
    def gen_town_names(town):
        """Generate town names."""
        yield town.strip()
        if town.find('.') != -1:
            town = town[town.index('.') + 1:].strip()
            yield town
        cleaned_town = town
        for pattern, replacement in conf.TOWN_SUBSTITUTIONS.items():
            cleaned_town = re.sub(pattern, replacement, cleaned_town, 0, re.IGNORECASE | re.UNICODE).strip()
        if cleaned_town != town:
            yield cleaned_town

    def gen_street_types(self, street):
        """Generate street types."""
        street_type, street_without_type = self.extract_part(street,
                                                             conf.STREET_TYPE_NAMES,
                                                             conf.OSM_STREET_TYPES_ALIASES)
        if street_type:
            yield street_type, street_without_type
        yield '', street_without_type
        yield conf.DEFAULT_OSM_STREET_TYPE, street_without_type

    def gen_street_names(self, street):
        """Generate street names."""
        street = street.strip()
        frozen_street = street
        # Extract district
        district, street = self.extract_part(street,
                                             conf.DISTRICT_NAMES,
                                             conf.DISTRICT_NAMES_ALIASES,
                                             False)
        district = ' ' + district if district else ''
        for street_type, street_without_type in self.gen_street_types(street):
            yield (street_type + ' ' if street_type else '') + street_without_type + district
            yield street_without_type + (' ' + street_type if street_type else '') + district
            cleaned_street = street_without_type
            for pattern, replacement in conf.STREET_SUBSTITUTIONS.items():
                cleaned_street = re.sub(pattern, replacement, cleaned_street, 0, re.IGNORECASE | re.UNICODE).strip()
            if cleaned_street != street_without_type:
                yield (street_type + ' ' if street_type else '') + cleaned_street + district
                yield cleaned_street + (' ' + street_type if street_type else '') + district
        # Special case for Saint-Petersburg
        for f in conf.SAINT_PETERSBURG_VO_LINES_FORMATS:
            m = re.match(f, street)
            if m:
                num = int(m.group(1))
                if num % 2 == 1:
                    num -= 1
                yield conf.SAINT_PETERSBURG_VO_DOUBLE_LINES_FORMAT % (str(num), str(num + 1)) + district
        yield frozen_street

    def town(self, town):
        """
        Spellcheck town name.

        :return: Correct town name if found. None if not.
        """
        raise NotImplementedError

    def street(self, town, street):
        """
        Spellcheck street name.

        :return: Correct street name if found. None if not.
        """
        raise NotImplementedError


class OsmNameCorrector(NameCorrector):
    """OpenStreetMap name corrector class."""

    def __init__(self, backend):
        super(OsmNameCorrector, self).__init__()
        config = GeocoderConfig()
        stats = GeocoderStats()
        self.gc_instance = backend.Geocoder(config, stats)

    def geocode(self, string, obj_type):
        """OSM geocoder corrections."""
        result = self.gc_instance.find_value(string)
        for item in result:
            if obj_type == self.TOWN:
                if (item['class'] == 'boundary' and item['type'] == 'administrative') or \
                        (item['class'] == 'place' and item['type'] == 'city') or \
                        (item['class'] == 'place' and item['type'] == 'suburb'):
                    return item['display_name']
            elif obj_type == self.STREET:
                if (item['class'] == 'highway' and not item['type'].endswith('_stop')) or \
                        (item['class'] == 'place' and item['type'] == 'suburb'):
                    return item['display_name']
        return None

    def town(self, town):
        """
        Search town in cache, if don't exists check town name via Nominatim.

        :return: found town name and is_cached.
        """
        if town in self.town_cache:
            return self.town_cache[town], True
        for name in self.gen_town_names(town):
            found_name = self.geocode(name, self.TOWN)
            if found_name:
                self.town_cache[town] = name  # Cache positive result
                return name, False
        self.town_cache[town] = None  # Cache negative result
        return None, False

    def street(self, town, street):
        """
        Search street in cache, if don't exists check its name via Nominatim.

        :return: found street name and is_cached.
        """
        full_name = town + ', ' + street
        if full_name in self.street_cache:
            return self.street_cache[full_name], True
        for name in self.gen_street_names(street):
            found_name = self.geocode(town + ', ' + name, self.STREET)
            if found_name:
                self.street_cache[full_name] = name  # Cache positive result
                return name, False
        self.street_cache[full_name] = None  # Cache negative result
        return None, False
