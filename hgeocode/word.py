"""Word class for Nominatim's words."""

import re
from collections import OrderedDict

from hgeocode import db
from hgeocode.syntax import synonyms


def cmp(a, b):
    """Return 0 if equal, 1 if first param is greater, otherwise return -1."""
    return (a > b) - (a < b)


GET_TOKEN_SQL = "select make_standard_name(%s)"
GET_WORD_SQL = """
    select word_id, search_name_count
    from word
    where word_token = ANY (%s)
"""
GET_TYPOS = """
    select *
    from (
        select word_id, word_token, search_name_count,
               levenshtein_less_equal(word_token, %(token)s, %(max_distance)s) as "dist"
        from word
        where length(word_token) < 256
              and word_token %% %(token)s
    ) s
    where dist <= %(max_distance)s
    order by dist, search_name_count
"""
GET_WORD_STAT = """
    select
      min(search_rank), max(search_rank),
      min(address_rank), max(address_rank),
      count(*)
    from search_name
    where %(words)s && name_vector
      and address_rank < 30
"""
GET_PLACES_BY_WORD_SQL = """
    select *,
      get_address_by_language(place_id, %(languages)s) as "full_address"
    from search_name
    where %(words)s && name_vector
      and address_rank <= 12
    order by address_rank
"""
GET_PLACES_BY_WORD_AND_PARENTS_SQL = """
    select *,
      get_address_by_language(place_id, %(languages)s) as "full_address"
    from (
        select distinct on (sn.place_id) sn.*
        from unnest(%(parents)s) p(id)
            , search_name sn
            , place_addressline pal
    where %(words)s && sn.name_vector
        and sn.place_id = pal.place_id and pal.address_place_id = p.id
        and sn.address_rank < 30
    ) sub
    order by address_rank
"""
GET_FULL_ADDRESS_SQL = """
    select get_address_by_language(%s, %s)
"""
GET_HOUSES_SQL = """
    select distinct on (housenumber, parent_place_id)
    place_id, parent_place_id, housenumber, street, isin
    from placex,
         unnest(%s) pl(id)
    where
        parent_place_id = pl.id
        and housenumber is not null
    order by housenumber
"""
GET_EXACT_HOUSE_SQL = """
    select distinct on (housenumber, parent_place_id)
        place_id, parent_place_id, housenumber, street, isin
    from placex,
         unnest(%(streets)s) pl(id)
    where
        parent_place_id = pl.id
        and (housenumber = %(house_number)s or housenumber ~ ('^\s*' || %(house_number)s || '[^\d].*'))
    order by housenumber
"""
GET_PLACES_SQL = """
    select place_id, parent_place_id, housenumber, street, isin
    from placex,
         unnest(%s) pl(id)
    where place_id = pl.id
"""


def split_into_words(query):
    """Split query string into words. Generate words."""
    # Finite state machine.
    # Possible states: s - space, l - letter, d - digit, p - punctuation marks.
    state = 's'  # current state
    word = ''  # current word
    prev_sep = ''  # separator before word
    sep = ''  # separator after word
    while query:
        char = query[0]
        query = query[1:]
        # Set new state according to new char
        if re.match('\s', char, re.UNICODE):
            new_state = 's'
        elif re.match('\d', char, re.UNICODE):
            new_state = 'd'
        elif re.match('\w', char, re.UNICODE) and char != '_':
            new_state = 'l'
        else:
            new_state = 'p'
        # Transitions
        if new_state == 'l' or new_state == 'd':
            if new_state != state:  # Beginning of the word
                if word:
                    yield {
                        'word': word,
                        'prev': prev_sep,
                        'next': sep
                    }
                word = ''
                prev_sep = sep
                sep = ''
        if new_state == 'l' or new_state == 'd':  # Add letter or digit to current word
            word += char
        if new_state == 's' or new_state == 'p':  # Add char to current separator
            sep += char
        state = new_state
    if word:  # Last word
        yield {
            'word': word,
            'prev': prev_sep,
            'next': sep
        }


class Word(object):
    """Word class for Nominatim's words."""

    def __init__(self, word, prev='', next='', order=None):
        if isinstance(word, dict):
            self.original = word['word']
            self.prev = word['prev']
            self.next = word['next']
        else:
            self.original = word
            self.prev = prev
            self.next = next
        self.order = order
        self.token = None
        self.ids = []
        self._full_ids = None
        self.typos = {}
        self.count = 0
        self.places = Places()
        self.parented_places = {}
        self.town = 0  # Importance level as part of the town name
        self.street = 0  # Importance level as part of the street name
        self.house = 0  # Importance level as part of the house number
        self.weight = len(self.original)

    def __unicode__(self):
        return self.original

    def get_token(self):
        """Get token from db or cached."""
        if self.token:
            return self
        dbc = db.DatabaseConnection()
        self.token = dbc.scalar(
            GET_TOKEN_SQL, (self.original,)
        )
        syn_tokens = []
        for synonym in synonyms(self.original, True):
            syn_tokens.append(dbc.scalar(
                GET_TOKEN_SQL, (synonym,)
            ))
        rows = dbc.row_array(GET_WORD_SQL, ([self.token] + syn_tokens,))
        self.ids = [row[0] for row in rows]
        self.count = sum(row[1] for row in rows)
        return self

    def get_typos(self, distance):
        """Get typos from db or cached."""
        if self.typos:
            return self
        dbc = db.DatabaseConnection()
        typos = dbc.dict_array(GET_TYPOS, {
            'token': self.token,
            'max_distance': distance
        })
        cur_dist = None
        for typo in typos:
            if cur_dist and typo['dist'] > cur_dist:
                break  # Use only minimum distance words
            cur_dist = typo['dist']
            self.typos[typo['word_id']] = typo['word_token']
            self.count += typo['search_name_count']
        return self

    def get_words_ids(self):
        """Combine ids with typos."""
        if self._full_ids is None:
            self._full_ids = set(self.ids + list(self.typos.keys()))
        return self._full_ids

    def get_statistics(self):
        """Get word stats from db or cached. Skip numbers."""
        if self.places.count is not None:
            return self
        if self.original.isdigit() or len(self.original) <= 2:
            # Don't analyse digits and short words for performance
            return self
        dbc = db.DatabaseConnection()
        row = dbc.row(GET_WORD_STAT, {'words': list(self.get_words_ids())})
        self.places.count = row[4]
        self.places.min_rank = row[2] or row[0] or 0
        self.places.max_rank = row[3] or row[1] or 0
        return self

    def get_places(self, lang_pref, parents=()):
        """Get places for word."""
        if not parents:
            # No parents - just return all places
            self.places.by_word(self, lang_pref)
            return self.places
        # Check if we have cached places for that parents
        par = tuple(sorted(list(parents)))
        if par in self.parented_places:
            return self.parented_places[par]
        # Don't have it - get it
        res = Places().by_word(self, lang_pref, par)
        self.parented_places[par] = res
        return res

    def copy(self, cached):
        """Copy data from cached instance."""
        self.token = cached.token
        self.ids = cached.ids
        self.typos = cached.typos
        self.count = cached.count
        self.places = cached.places

    @staticmethod
    def cmp_by_importance(w1, w2):
        """Compare two street words by ranks of its places and its originality."""
        result = w2.street - w1.street
        if result:
            return cmp(result, 0)
        result = (w2.places.count is not None) - (w1.places.count is not None)
        if result:
            return cmp(result, 0)
        result = (w2.places.max_rank > 25) - (w1.places.max_rank > 25)
        if result:
            return cmp(result, 0)
        result = (w1.places.count or 0) - (w2.places.count or 0)
        if result:
            return cmp(result, 0)
        result = w2.places.min_rank - w1.places.min_rank
        if result:
            return cmp(result, 0)
        result = w2.weight - w1.weight
        if result:
            return cmp(result, 0)
        return cmp(w1.count, w2.count)

    @staticmethod
    def cmp_by_town_importance(w1, w2):
        """Compare two town words by ranks of its places and its originality."""
        result = w2.town - w1.town
        if result:
            return cmp(result, 0)
        result = (w2.places.min_rank >= 8) - (w1.places.min_rank >= 8)
        if result:
            return cmp(result, 0)
        result = w1.places.count - w2.places.count
        if result:
            return cmp(result, 0)
        result = w2.weight - w1.weight
        if result:
            return cmp(result, 0)
        return cmp(w1.count, w2.count)


class Place(object):
    """Place class for Nominatim's places."""

    def __init__(self, **kwargs):
        self.id = kwargs.get('place_id')
        self.name_vector = kwargs.get('name_vector')
        self.name_address_vector = kwargs.get('nameaddress_vector')
        self.name_set = None
        self.rank = kwargs.get('address_rank')

        self.place_class = kwargs.get('class')
        self.type = kwargs.get('type')
        self.admin_level = kwargs.get('admin_level')
        self.parent_place_id = kwargs.get('parent_place_id')
        self.name = kwargs.get('name')
        self.full_address = kwargs.get('full_address')
        self.address_words = None

        self.grade = None
        self.absent_words = set()

    def get_full_address(self, lang_pref):
        """Get full address from db."""
        if not self.full_address:
            dbc = db.DatabaseConnection()
            self.full_address = dbc.scalar(GET_FULL_ADDRESS_SQL, (self.id, lang_pref))
        return self.full_address

    def set_absent_words(self, words):
        """Set absent words."""
        self.absent_words = set(words)

    def has_word(self, word):
        """Check if place contains word."""
        if self.name_set is None:
            self.name_set = set(self.name_vector) | set(self.name_address_vector)
        if word.get_words_ids() & self.name_set:
            return True
        if self.address_words is None:
            self.address_words = {part['word'] for part in split_into_words(self.full_address.lower())}
        if word.original.lower() in self.address_words:
            return True
        if synonyms(word.original, True) & self.address_words:
            return True
        return False


class Places(OrderedDict):
    """Places hierarchical structure."""

    def __init__(self, *args, **kwargs):
        super(Places, self).__init__(*args, **kwargs)
        self.from_db = False
        self.min_rank = 0
        self.max_rank = 0
        self.count = None

    def by_word(self, word, lang_pref, parents=()):
        """Get place from db by word."""
        if self.from_db:
            return self
        dbc = db.DatabaseConnection()
        sql = GET_PLACES_BY_WORD_AND_PARENTS_SQL if parents else GET_PLACES_BY_WORD_SQL
        places = dbc.dict_array(sql, {
            'words': list(word.get_words_ids()),
            'languages': lang_pref,
            'parents': list(parents),
        })
        self.clear()
        self.min_rank = 0
        self.max_rank = 0
        for d in places:
            self[d['place_id']] = Place(**d)
        self.count = len(places)
        if places:
            self.min_rank = places[0]['address_rank'] or places[0]['search_rank']
            self.max_rank = places[-1]['address_rank'] or places[-1]['search_rank']
        self.from_db = True
        return self

    def set_absent_words(self, words):
        """Set absent words."""
        for place in self.values():
            place.set_absent_words(words)

    def intersect(self, a, b):
        """Intersect."""
        self.clear()
        for key in a.keys():
            if key in b:
                self[key] = a[key]
        return self

    def filter_by_word(self, word):
        """Filter buy word."""
        for key in [key for key, place in self.items() if not place.has_word(word)]:
            del self[key]
        return self

    def check_word(self, word):
        """Check if place in misplaced in absent_words."""
        for key, place in self.items():
            if place.has_word(word):
                place.absent_words.remove(word)
        return self

    def filter_by_rank(self, min_rank=0, max_rank=30):
        """Filter by rank."""
        for key in [key for key, place in self.items() if place.rank < min_rank or place.rank > max_rank]:
            del self[key]
        return self

    def get_houses(self):
        """Get houses from db."""
        dbc = db.DatabaseConnection()
        return dbc.dict_array(GET_HOUSES_SQL, (list(self.keys()),))

    def get_exact_house(self, house_number):
        """Get exact house from db."""
        dbc = db.DatabaseConnection()
        return dbc.dict_array(GET_EXACT_HOUSE_SQL, {
            'streets': list(self.keys()),
            'house_number': house_number
        })

    def get_places(self):
        """Get places from db."""
        dbc = db.DatabaseConnection()
        return dbc.dict_array(GET_PLACES_SQL, (list(self.keys()),))
