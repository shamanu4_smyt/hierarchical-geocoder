"""Working with PostgreSQL database."""

import logging

import psycopg2
from psycopg2.extensions import adapt

from hgeocode import conf

logger = logging.getLogger('nominatim')


class DatabaseConnection(object):
    """
    Provide some additional methods for working with PostgreSQL database.

    We want cache database connection between executions.
    In order to prevent reconnections to database DatabaseConnection is singleton.
    """

    _conn = None

    def __new__(cls):
        """Make singleton."""
        if not hasattr(cls, 'instance'):
            cls.instance = super(DatabaseConnection, cls).__new__(cls)
        return cls.instance

    def ping(self):
        """Run silently if db connection is alive."""
        self.perform('SELECT 1')

    def connect(self):
        """Connect to database."""
        if self._conn is not None:
            self._conn.close()
        logger.info('Connect to database.')
        self._conn = psycopg2.connect(conf.DATABASE_CONNECTION_STRING)
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE, self._conn)
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY, self._conn)

    def check(self):
        """Check if db connection is alive."""
        try:
            self.ping()
        except psycopg2.Error:
            logger.warning('Database ping failed. Reconnection...')
            self.connect()

    @property
    def conn(self):
        """Get database connection handler."""
        if self._conn is None:
            self.connect()
        return self._conn

    def adapt(self, value):
        """Escape value for usage in SQL statement."""
        adapted = adapt(value)
        if hasattr(adapted, 'prepare'):
            adapted.prepare(self.conn)
        return adapted

    def _execute_query(self, query, func=None, args=()):
        """Execute SQL query. Call func on cursor and return func's result."""
        logger.debug(query % args)
        result = None
        with self.conn.cursor() as curs:
            curs.execute(query, args)
            if func is not None:
                result = func(curs)
        return result

    def perform(self, query, args=()):
        """Execute SQL query. Return nothing."""
        return self._execute_query(query, None, args)

    def scalar(self, query, args=()):
        """Execute SQL query and return a scalar value."""
        return self._execute_query(query, lambda cur: cur.fetchone()[0], args)

    def row(self, query, args=()):
        """Execute SQL query and return one row."""
        return self._execute_query(query, lambda cur: cur.fetchone(), args)

    def row_as_dict(self, query, args=()):
        """Execute SQL query and return one row as dict."""
        def process(cur):
            columns = [col.name for col in cur.description]
            row = cur.fetchone()
            result = {k: v for k, v in zip(columns, row)}
            return result

        return self._execute_query(query, process, args)

    def scalar_array(self, query, args=()):
        """Execute SQL query and return array of scalars."""
        return self._execute_query(query, lambda cur: [row[0] for row in cur], args)

    def row_array(self, query, args=()):
        """Execute SQL query and return all rows."""
        return self._execute_query(query, lambda cur: cur.fetchall(), args)

    def dict_array(self, query, args=()):
        """Execute SQL query and return all rows as dicts."""
        def process(cur):
            result = []
            columns = [col.name for col in cur.description]
            for row in cur:
                d = {k: v for k, v in zip(columns, row)}
                result.append(d)
            return result

        return self._execute_query(query, process, args)
