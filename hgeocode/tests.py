# -*- coding:utf-8 -*-

import unittest

from hgeocode import hierarchical, name_corrector
from hgeocode.geocoder import GeocoderConfig, GeocoderStats

osm_corr = name_corrector.OsmNameCorrector(hierarchical)

config = GeocoderConfig(None, osm_corr)
stats = GeocoderStats()


class HierarchicalGeocodeTest(unittest.TestCase):
    """Test hierarchical geocoder class."""

    gc = None

    @classmethod
    def setUpClass(cls):
        """Instantiate geocoder for all tests."""
        cls.gc = hierarchical.Geocoder(config, stats)

    def test_00_init(self):
        """Check geocoder does not throw any errors."""
        self.gc.geocode(town="Санкт-Петербург", address="пр-кт Невский, 55")
