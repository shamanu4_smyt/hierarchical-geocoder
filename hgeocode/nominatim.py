# encoding: utf-8
"""Hierarchical geocoder for Nominatim's database."""

import copy
import logging
from functools import cmp_to_key

from hgeocode import details, syntax
from hgeocode.word import split_into_words, Word, Places

# Match street and home importance to all of supported structured query address parts
STRUCTURED_QUERY_PARTS = {
    'address': (None, None, None),
    'town': (1, 0.1, 0),
    'street': (0, 1, 0),
    'house': (0, 0, 1),
    'build': (0, 0, 1),
    'postalcode': (0, 0.5, 0),
    'region': (0.5, 0.7, 0),
    'village': (0.7, 0.5, 0),
}

PREFERRED_LANGUAGES = [
    'short_name:ru', 'name:ru', 'place_name:ru', 'official_name:ru',
    'short_name', 'name', 'place_name', 'official_name', 'ref', 'type'
]

logger = logging.getLogger('nominatim')


class HGeocode(object):
    """Hierarchical geocoder for Nominatim's database."""

    preferred_languages = PREFERRED_LANGUAGES

    def __init__(self):
        self.word_cache = {}

    def set_language_preference(self, lang_pref):
        """Set language preference."""
        self.preferred_languages = copy.deepcopy(lang_pref)

    def search(self, query='', **kwargs):
        """
        Search places and return list.

        :param query: address can be specified by one string in 'query' parameter
        :param kwargs: structured form of address
        :return:
        """
        words, house_number = self.analyse_query({'address': query} if query else kwargs)

        towns = self.get_towns(words)

        streets = self.get_streets(words, towns)
        if not streets:
            return []

        if house_number:
            logger.info('House number: %s' % (house_number.original,))
            houses = self.get_houses(streets, house_number)
        else:
            houses = []

        # Format search result
        if houses:
            results = [house['place_id'] for house in houses]  # House was found
        else:
            results = []  # streets.keys()     # Street was found
        results = details.get_details(results, self.preferred_languages)
        results = details.format_results(results)
        return results

    def analyse_query(self, query_parts):
        """Split query into words, analyse each word and extract house number."""
        words = []
        house_words = []
        for name, part in query_parts.items():
            if name not in STRUCTURED_QUERY_PARTS:
                continue
            town, street, house = STRUCTURED_QUERY_PARTS[name]
            part_words = [Word(part, order=index) for index, part in enumerate(split_into_words(part))]
            for word in part_words:
                self.get_word(word)
            if town is not None and street is not None and house is not None:
                for word in part_words:
                    word.town = town
                    word.street = street
                    word.house = house
            else:
                syntax.analyze(part_words)
            words.extend(part_words)
            if name == 'house':
                house_words = part_words
            elif name == 'address' and not house_words:
                house_words = part_words
        logger.info('Words: %s' % (', '.join([word.original for word in words])))
        logger.info('Tokens: %s' % (', '.join([word.token for word in words])))

        house_number = self.house_number(house_words)

        return words, house_number

    @staticmethod
    def house_number(words):
        """Get a house number from given address."""
        result = None
        new_block = True
        for word in words:
            if word.street == 1:
                new_block = True
            if new_block and word.house == 1:
                result = word
                new_block = False
        return result

    def get_word(self, word):
        """Word caching for performance."""
        if word.original in self.word_cache:
            logger.info('analyzing word: %s (cache)' % (word.original,))
            cached = self.word_cache[word.original]
            word.copy(cached)
        else:
            logger.info('analyzing word: %s (DB)' % (word.original,))
            word.get_token()
            if not word.ids and len(word.original) > 3:
                # Exact word matching was not found, try to find typos.
                dist = 1 + int((len(word.token) - 3) / 4)
                word.get_typos(dist)
            word.get_statistics()
            # Save in cache
            self.word_cache[word.original] = word
        return word

    def get_places(self, all_words, level_words, level, parents=(), min_rank=0, max_rank=30):
        """
        Get a list of places for given list of names and level.

        Get ('town' or 'street'), located in the parents places.
        """
        best_weight = 0
        best_places = Places()
        best_present_words = []
        # We can iterate over words and find the best word,
        # but we sorted the words by importance and usually the first word is the best.
        for main_word in level_words:
            if best_places and (getattr(main_word, level) < 1 or len(main_word.original) < 3):
                continue  # Don't check low importance words when places already found
            if main_word in best_present_words:
                continue  # We already found this word
            # Try use this word as place's name
            logger.info('Main word: %s' % (main_word.original,))
            main_places = main_word.get_places(self.preferred_languages, parents).copy()
            main_places.filter_by_rank(min_rank, max_rank)
            if main_places:
                present_words = [main_word]
                # Filtering by other words
                for word in level_words:
                    if word == main_word or getattr(word, level) < 1:  # Only if it is surely name of given level
                        continue
                    places = main_places.copy().filter_by_word(word)
                    if places:
                        main_places = places
                        present_words.append(word)
                weight = sum(word.weight * getattr(word, level) for word in present_words)
                if weight > best_weight:
                    best_weight = weight
                    best_present_words = present_words
                    best_places = main_places
        if not best_places:
            return Places()

        absent_words = [word for word in all_words if word not in best_present_words]
        logger.info('Present: %s' % (', '.join([word.original for word in best_present_words])))
        logger.info('Absent: %s' % (', '.join([word.original for word in absent_words])))
        # Search for other words
        logger.info('Setting absent words')
        best_places.set_absent_words(absent_words)
        logger.info('Checking absent words')
        for word in absent_words:
            if getattr(word, level) == 0:
                continue
            best_places.check_word(word)

        # Calculate grades
        logger.info('Calculating grades')
        total_words = len(level_words)
        for place in list(best_places.values()):
            absent_words_count = sum(1 for word in place.absent_words if word in level_words)
            place.grade = float(total_words - absent_words_count) / total_words

        # logger.info('Absent: %s' % (', '.join([word.original for word in absent_words])))
        for place in list(best_places.values()):
            logger.info('Grade: %s, address: %s' %
                        (place.grade, place.get_full_address(self.preferred_languages)))

        return best_places

    def get_towns(self, words):
        """Get town objects for given name words."""
        town_words = [word for word in words if word.town > 0 and word.town >= word.street and word.house == 0]
        town_words.sort(key=cmp_to_key(Word.cmp_by_town_importance))
        logger.info('Town words: %s' % (', '.join([word.original for word in town_words])))
        return self.get_places(words, town_words, 'town', (), 8, 12)

    def get_streets(self, words, towns):
        """Get streets for names list in words."""
        # If towns is given then some words are town names, get other words from towns
        if towns:
            words = set()
            for place in list(towns.values()):
                words |= place.absent_words
        # There are no town names now
        for word in words:
            word.street = min(word.town + word.street, 1)
        street_words = [word for word in words
                        if (word.street > 0 and word.street >= word.house) or
                        (word.town > 0 and word.town >= word.house)]
        street_words.sort(key=cmp_to_key(Word.cmp_by_importance))
        logger.info('Street words: %s' % (', '.join([word.original for word in street_words])))
        return self.get_places(words, street_words, 'street', towns, 20)

    @staticmethod
    def get_houses(streets, house_number_word):
        """Get houses list for found street."""
        # Try to get exact house number
        target_house_number = house_number_word.original
        houses = streets.get_exact_house(house_number_word.original)
        logger.info('%s exact houses' % (len(houses),))
        if not houses:
            # Try to get all houses on the street
            houses = streets.get_houses()
            logger.info('%s houses on the street' % (len(houses),))
        found_houses = []
        left = None
        right = None
        for house in houses:
            logger.info('%s, %s, %s' % (house['housenumber'], house['street'], house['isin']))
            house_number = house['housenumber']
            house_words = [part['word'] for part in split_into_words(house_number)]
            if not house_words:
                continue
            main_house_number = house_words[0].lower()
            street = streets[house['parent_place_id']]
            absent_words = [word.original.lower() for word in street.absent_words]
            if main_house_number == target_house_number or main_house_number in absent_words:
                absent_words.remove(main_house_number)
                count = 1
                for word in house_words[1:]:
                    if word.lower() in absent_words:
                        absent_words.remove(word.lower())
                        count += 1
                grade = float(count) / len(street.absent_words)
                house['grade'] = street.grade * grade
                found_houses.append(house)
                logger.info('Good. grade: %s' % grade)
            # Find closest right and left neighbour house
            if target_house_number.isdigit() and main_house_number.isdigit():
                delta = int(target_house_number) - int(main_house_number)
                if delta % 2 == 0:
                    house['delta'] = delta
                    if delta > 0:
                        if not left or left['delta'] > delta:
                            left = house
                    else:
                        if not right or right['delta'] < delta:
                            right = house
        found_houses.sort(key=lambda house: house['grade'], reverse=True)
        logger.info('%s houses was found' % (len(found_houses),))
        if not found_houses:
            # Use neighbours
            if left:
                logger.info('Left house: %s, %s, %s' % (left['housenumber'], left['street'], left['isin']))
                left['neighbour'] = 'left'
                found_houses.append(left)
            if right:
                logger.info('Right house: %s, %s, %s' % (right['housenumber'], right['street'], right['isin']))
                right['neighbour'] = 'right'
                found_houses.append(right)
            found_houses.sort(key=lambda house: abs(house['delta']))
            logger.info('%s neighbour houses was found' % (len(found_houses),))
        return found_houses


def search(query='', **kwargs):
    """
    Geocode string.

    :param query: query string with full address
    :param kwargs: structured query
    :return: list of places
    """
    geocode = HGeocode()

    # lang_pref_order = lib.get_preferred_languages()
    # geocode.set_language_preference(lang_pref_order)
    geocode.include_polygon_as_geo_json = True

    result = geocode.search(query, **kwargs)

    return result
