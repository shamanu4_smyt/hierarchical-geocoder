# coding=utf-8
from collections import OrderedDict

NOMINATIM_DATABASE = {
    'NAME': 'nominatim',
    'USER': 'db_user',
    'PASSWORD': 'db_password',
    'HOST': 'localhost',
    'PORT': '5432'
}

DATABASE_CONNECTION_STRING = " ".join("{}={}".format(key, value) for key, value in {
    'host': NOMINATIM_DATABASE.get('HOST'),
    'dbname': NOMINATIM_DATABASE.get('NAME'),
    'user': NOMINATIM_DATABASE.get('USER'),
    'password': NOMINATIM_DATABASE.get('PASSWORD'),
    'port': NOMINATIM_DATABASE.get('PORT'),
}.items() if value)


SEARCH_AREA_POLYGONS = True

DEFAULT_OSM_STREET_TYPE = 'улица'

DEFAULT_STREET_TYPE_NAMES = ['аллея',
                             'б-р', 'бул', 'бульв', 'бульвар',
                             'наб', 'набережная',
                             'пр', 'просп', 'пр-кт', 'проспект', 'пропсект',
                             'проулок', 'пер', 'переулок',
                             'ул', 'улица',
                             'проезд',
                             'шоссе', 'ш',
                             'дор', 'дорога', 'д',
                             'площадь', 'пл.', 'пл']

STREET_TYPE_NAMES = DEFAULT_STREET_TYPE_NAMES

OSM_STREET_TYPES_ALIASES = {
    ('пер',): 'переулок',
    ('ул',): 'улица',
    ('наб',): 'набережная',
    ('пл.', 'пл'): 'площадь',
    ('пр', 'просп', 'пропсект', 'пр-кт'): 'проспект',
    ('ш',): 'шоссе',
    ('б-р', 'бул', 'бульв',): 'бульвар',
    ('д', 'дор',): 'дорога'
}

DISTRICT_NAMES = [
    r'\bВ\.О\.', r'\bВО\b',
    r'\bП\.С\.',
]

DISTRICT_NAMES_ALIASES = {
    ('во',): 'В.О.',
}

TOWN_SUBSTITUTIONS = OrderedDict([
    (',', ' '),
    (r'\bпос\s+', ''),
    (r'Петродворец', 'Петергоф'),
])

STREET_SUBSTITUTIONS = OrderedDict([
    (r'\bканал\b\s*', ''),
    (r'\bкан\b\.?\s*', ''),
    (r'\b[а-яА-Я]+\.\s*', ''),
    ('\(.*\)', ''),
    (r',', ' '),
    (r'\.', ' '),

    (r'-ая\s', '-я '),
])

ADDRESS_SUBSTITUTIONS = OrderedDict([
    (r'\b(корп|кор|к)\.\s*', 'к'),
    (r'\b(лит|литер)\.\s*', ''),
    (r'\bлитер\s*', ' '),
    (r'\bстр\.\s*', ''),
    ('\(.*\)', ''),
    (',', ' '),
])

SAINT_PETERSBURG_VO_LINES_FORMATS = [
    r'^\s*(\d+)-я\s+линия\s*$',
    r'^\s*(\d+)-я\s*$'
]
SAINT_PETERSBURG_VO_DOUBLE_LINES_FORMAT = '%s-%s-я линии'

ADDRESS_LITERA = [
    r'(лит|литер)\.\s*',
    r'\sлитер\s*',
]
